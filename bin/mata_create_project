#!/usr/bin/env python3

import sys
import os
import shutil
from glob import glob

script_ending = '.bat' if sys.platform == 'win32' else '.sh'

args = sys.argv[1:]
base_dir = os.getcwd()

config = {
            'platform'      : sys.platform,
            'script_ending' : script_ending,
            'setup_level'   : 'sample',
            'cython'        : True,
            'helper_scripts': True,
            'launcher'      : True,
            'project_name'  : ''
         }

# Handle the keyword arguments here
for arg in args:
    # It's a long keyword argument
    if arg.startswith('--'):
        # Define the project environment
        # minimal has an empty mods folder, with no launcher and no scripts
        # dev includes helper scripts, launcher and empty mods folder
        # sample is essentially a clone of Gitlab
        if arg == '--bare':
            config['setup_level'] = 'minimal'
            config['launcher'] = False
            config['helper_scripts'] = False
        if arg == '--dev-env':
            config['setup_level'] = 'dev'
            config['launcher'] = True
            config['helper_scripts'] = True

        # Disable Cython
        if arg == '--no-cython':
            config['cython'] = False

        # Disable the helper scripts (pythonify_api, setup, etc)
        if arg == '--no-helpers':
            config['helper_scripts'] = False

        # Remove the launcher
        if arg == '--no-launcher':
            config['launcher'] = False

    else:
        # It's the project name
        config['project_name'] = arg

def copy_file(src_dir, filename, dst_dir):
    shutil.copy(src_dir + os.path.sep + filename, dst_dir)

if config.get('project_name'):
    # Setup the project (assume basic files are in, mata.__file__)
    print('Generating project folder...')

    # Setup the root folder
    try:
        os.mkdir(config['project_name'])
    except:
        print('[WARNING] A folder already exists with specified project name.')
        print('If you continue, folder contents may be overwritten.')
        if input('Are you sure you wish to continue? [Y/n]').lower() == 'n':
            sys.exit()
    dst_dir = base_dir + os.path.sep + config['project_name']

    # Check that the user has the mata engine installed from PyPI
    try:
        import mata
    except:
        print('[FATAL] M.A.T.A engine is not installed. Please run "pip install mata".')
        sys.exit()

    # Switch into the package source folder
    src_folder = mata.__file__.split(os.path.sep)[:-1]
    src_folder = os.path.sep.join(src_folder)
    os.chdir(src_folder)

    # Start copying the files over
    # At a minimum, copy the api folder, main.py, mod.py, game.py, config, modlist, util.py and icon.png
    files_base = ['main.py', 'mod.py', 'game.py', 'config', 'util.py', 'icon.png']
    for filename in files_base:
        copy_file(src_folder, filename, dst_dir)

    # Create the api folder
    shutil.copytree(src_folder + os.path.sep + 'api', dst_dir + os.path.sep + 'api')

    # Convert Cython modules to Python if necessary
    if not config.get('cython', True):
        # Copy the pythonify script, then delete it.
        copy_file(src_folder, 'pythonify_api' + script_ending, dst_dir)
        os.chdir(dst_dir)
        os.system(dst_dir + os.path.sep + 'pythonify_api' + script_ending)
        os.remove('pythonify_api' + script_ending)
        os.chdir(src_folder)

    # Copy the launcher if possible
    if config.get('launcher'):
        copy_file(src_folder, 'launcher.py', dst_dir)

    # Copy the helper scripts if they are wanted
    if config.get('helper_scripts'):
        files_scripts = glob(src_folder + os.path.sep + '*' + script_ending)
        for filename in files_scripts:
            shutil.copy(filename, dst_dir)

        copy_file(src_folder, 'compile_api.py', dst_dir)

    # If creating a minimal project, initialise an empty mods and resources folder
    if config.get('setup_level') != 'sample':
        os.chdir(dst_dir)
        os.mkdir('mods')
        os.mkdir('resources')
        copy_file(src_folder, 'modlist_blank', dst_dir + os.path.sep + 'modlist')
        os.chdir(src_folder)

    # Copy the entire mods and resources folder from the src_folder
    else:
        copy_file(src_folder, 'modlist', dst_dir)
        shutil.copytree(src_folder + os.path.sep + 'mods', dst_dir + os.path.sep + 'mods')
        shutil.copytree(src_folder + os.path.sep + 'resources', dst_dir + os.path.sep + 'resources')

    # Print a nice message
    print('Project generation complete.')

else:
    print('[FATAL] No project name specified.')
