"""
This is the gui subpackage of the api package.

This package contains two main modules, the objects module and the gui module.

The objects module contains several high-level elements which can be incorporated into a Gui easily, and with minimal customisation or setup required. They are especially useful for menu design.

The gui module contains all classes relating to the Gui handling within the engine. Most Guis made will extend one or more classes within the gui module.
"""
