"""
This is the api package.

This package contains all of the modules for the M.A.T.A engine's API, as well as the gui subpackage. The gui subpackage contains modules relating to gui development within the M.A.T.A engine.

Modules are separated into logical blocks based on functionality. Modules may depend upon one another, so it is best for the entire API to be included in any distribution.
"""
